// This is the 'extension.js' file

const vscode = require('vscode');

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {

    let disposable = vscode.commands.registerCommand('fox.changeTheme', function () {
        // The code you place here will be executed every time your command is executed

        // Change the color theme to 'Monokai'
        vscode.workspace.getConfiguration().update('workbench.colorTheme', 'Monokai', true);
    });

    context.subscriptions.push(disposable);
}

exports.activate = activate;

function deactivate() {}

module.exports = {
    activate,
    deactivate
}